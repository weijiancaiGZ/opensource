CC = gcc
CPP = g++

#文件夹路径
ROOTPATH=.
INCLUDE = -I./cfg/inc -I$(ROOTPATH)/easylogger/inc -I$(ROOTPATH)/easylogger/plugins/file
LIB=-lpthread

# c文件和c++文件混编要加
LDFLAGS=-lstdc++			

#.c文件目录
COBJS += $(patsubst %.c, %.o, $(wildcard *.c))
COBJS += $(patsubst %.c, %.o, $(wildcard $(ROOTPATH)/easylogger/src/*.c))
COBJS += $(patsubst %.c, %.o, $(wildcard $(ROOTPATH)/easylogger/plugins/file/elog_file.c))
COBJS += $(patsubst %.c, %.o, $(wildcard easylogger/port/*.c))
COBJS += $(patsubst %.c, %.o, $(wildcard cfg/port/*.c))

#.cpp文件目录
CPPSRCS:=$(wildcard *.cpp ./*.cpp) 
CPPOBJS := $(CPPSRCS:.cpp=.o)

# 目标文件
BIN=EasyLoggerLinuxDemo

CFLAGS = -O0 -g3 -Wall

all:$(BIN)

# c目标文件生成
$(COBJS) : %.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@ $(INCLUDE)
	mv $@ out

# cpp目标文件生成
$(CPPOBJS) : %.o: %.cpp
	$(CPP) $(CFLAGS) -c $< -o $@ $(INCLUDE)
	mv $@ out

# 目标文件生成依赖
$(BIN):$(COBJS) $(CPPOBJS)
	$(CC) out/*.o $(CFLAGS) $(LDFLAGS) $(LIB) -o $(BIN) 
	mv $(BIN) out

clean:
	rm -rf out/*

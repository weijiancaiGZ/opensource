# 开源项目

优秀开源项目

本项目实现自定义的打印格式，并且输出内容到log里面时，不将颜色部分的字符写日志，实现了终端可以根据不同等级的打印显示不同的颜色字符输出，同时可以将内容输出到日志;

其中:
cfg/inc/elog_cfg.h 配置宏来开启是否需要打开功能;
cfg/inc/elog_file_cfg.h  配置日志文件的路径以及大小等信息

同时: 
写文件和终端输出需要重载easylogger目录下的port/elog_port.c 以及plugin/file/elog_file_port.c, 才能实现功能， 即是:
cfg/port/elog_file_port.c
cfg/port/elog_port.c


参考：https://github.com/armink/EasyLogger



